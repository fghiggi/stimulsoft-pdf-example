﻿using Microsoft.AspNetCore.Mvc;

using Stimulsoft.Base.Drawing;
using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Mvc;

using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

namespace stimulsoft_pdf_example.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ExportPdf1()
        {
            _logger.LogInformation("Generating report with 1 page.");
            return ResponseAsPdf(GetReport(reportPages: 1));
        }

        public IActionResult ExportPdf100()
        {
            _logger.LogInformation("Generating report with 100 pages.");
            return ResponseAsPdf(GetReport(reportPages: 100));
        }

        public IActionResult ExportPdf500()
        {
            _logger.LogInformation("Generating report with 500 pages.");
            return ResponseAsPdf(GetReport(reportPages: 500));
        }

        public IActionResult ExportPdf1000()
        {
            _logger.LogInformation("Generating report with 1000 pages.");
            return ResponseAsPdf(GetReport(reportPages: 1000));
        }

        private StiNetCoreActionResult ResponseAsPdf(StiReport report)
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();

            var response = StiNetCoreReportResponse.ResponseAsPdf(report, saveFileDialog: false, settings: new StiPdfExportSettings()
            {
                ImageResolution = 600
            });

            stopwatch.Stop();

            _logger.LogInformation($"Elapsed time: {stopwatch.Elapsed}");

            return response;
        }

        private StiReport GetReport(int reportPages)
        {
            var reportPath = StiNetCoreHelper.MapPath(this, "Reports/Report.mrt");
            var report = new StiReport();

            report.Load(reportPath);

            SetVariables();
            SetFuncionarioDatatable();
            SetDiasDataTable();

            return report;

            void SetVariables()
            {
                using var image = Image.FromFile("Images/logotipo.png");

                using var ms = new MemoryStream();
                image.Save(ms, ImageFormat.Jpeg);
                var imageBytes = ms.ToArray();

                var logotipo = StiImageConverter.StringToImage(Convert.ToBase64String(imageBytes));

                report.Dictionary.Variables.Add("LogoEmpresa", logotipo);

                foreach (DictionaryEntry variable in ReportVariables)
                {
                    report.Dictionary.Variables.Add(variable.Key.ToString(), variable.Value);
                }
            }

            void SetFuncionarioDatatable()
            {
                var dataTableFuncionario = new DataTable();

                dataTableFuncionario.Columns.Add("funcionario_id", typeof(long));
                dataTableFuncionario.Columns.Add("nome", typeof(string));

                for (var i = 1; i <= reportPages; i++)
                {
                    var novaLinha = dataTableFuncionario.NewRow();

                    novaLinha["funcionario_id"] = i;
                    novaLinha["nome"] = string.Concat(Enumerable.Repeat(i, 10));

                    dataTableFuncionario.Rows.Add(novaLinha);
                }

                dataTableFuncionario.TableName = "funcionario";

                report.RegData("funcionario", dataTableFuncionario);
            }

            void SetDiasDataTable()
            {
                var dataTableDias = new DataTable();

                dataTableDias.Columns.Add("funcionario_id", typeof(long));
                dataTableDias.Columns.Add("dia", typeof(string));
                dataTableDias.Columns.Add("campo1", typeof(string));
                dataTableDias.Columns.Add("campo2", typeof(string));
                dataTableDias.Columns.Add("campo3", typeof(string));
                dataTableDias.Columns.Add("campo4", typeof(string));
                dataTableDias.Columns.Add("campo5", typeof(string));
                dataTableDias.Columns.Add("campo6", typeof(string));
                dataTableDias.Columns.Add("campo7", typeof(string));
                dataTableDias.Columns.Add("campo8", typeof(string));
                dataTableDias.Columns.Add("campo9", typeof(string));
                dataTableDias.Columns.Add("campo10", typeof(string));
                dataTableDias.Columns.Add("data", typeof(string));

                for (var i = 1; i <= reportPages; i++)
                {
                    var novaLinha = dataTableDias.NewRow();

                    novaLinha["funcionario_id"] = i;
                    novaLinha["dia"] = "TOTAIS";
                    novaLinha["campo1"] = "TOTAIS";
                    novaLinha["campo2"] = "";
                    novaLinha["campo3"] = "";
                    novaLinha["campo4"] = "";
                    novaLinha["campo5"] = "07:59";
                    novaLinha["campo6"] = "00:01";
                    novaLinha["campo7"] = "00:00";
                    novaLinha["campo8"] = "00:00";
                    novaLinha["campo9"] = "00:00";

                    dataTableDias.Rows.Add(novaLinha);

                    novaLinha = dataTableDias.NewRow();

                    novaLinha["funcionario_id"] = i;
                    novaLinha["dia"] = "22/03/2023 - Qua";
                    novaLinha["campo1"] = "06:22";
                    novaLinha["campo2"] = "13:23";
                    novaLinha["campo3"] = "14:24";
                    novaLinha["campo4"] = "15:22";
                    novaLinha["campo5"] = "07:59";
                    novaLinha["campo6"] = "00:01";

                    dataTableDias.Rows.Add(novaLinha);
                }

                report.RegData("dias", dataTableDias);
            }
        }

        Hashtable ReportVariables = new()
        {
            {"Emitido", "Emitido em"},
            {"nome_coluna7", "EX50%"},
            {"Ent", "ENTRADA"},
            {"Sex", "SEXTA"},
            {"nome_coluna4", "SAÍ. 2"},
            { "Ter", "TERÇA"},
            { "Data", "DATA"},
            { "rodape_texto", "Reconheço a exatidão das horas constantes de acordo com minha frequência neste intervalo 21/03/2023 à 20/04/2023. Nos termos da Portaria MTB No. 3626 de 13/11/91 artigo 13, o presente Cartão Ponto substitui o quadro de horário de Trabalho. Inclusive Ficha de Horário de Trabalho Externo."},
            { "Departamento", "DEPARTAMENTO"},
            { "NumeroPis", "Nº PIS/PASEP"},
            { "Estrutura", "ESTRUTURA"},
            { "Funcao", "FUNÇÃO"},
            { "VersaoAssinatura", 0},
            { "LogoSubtitulo", "ponto"},
            { "Seg", "SEGUNDA"},
            { "ContadorPaginaDe", "de"},
            { "Dia", "DIA"},
            { "nome_coluna6", "FALTAS"},
            { "nome_coluna2", "SAÍ. 1"},
            { "Sai", "SAÍDA"},
            { "nome_coluna3", "ENT. 2"},
            { "Qua", "QUARTA"},
            { "DezBatidas", true},
            { "Qui", "QUINTA"},
            { "ObservacoesTitulo", "OBSERVAÇÕES"},
            { "LegendaCiclos", "LEGENDA DOS CICLOS"},
            { "Subtitulo", "Período: 21/03/2023 até 20/04/2023"},
            { "PessoaObservacao", "OBSERVAÇÃO"},
            { "Sab", "SÁBADO"},
            { "HorarioTrabalho", "HORÁRIO DE TRABALHO"},
            { "nome_coluna9", "NOT.TOT."},
            { "MensagemExtras", "Coluna Horas Extras suprimida manualmente pelo usuário"},
            { "AvisoCLT", "Secullum"},
            { "NomeSistema", "Secullum Ponto Web"},
            { "nome_coluna5", "NORMAIS"},
            { "Dom", "DOMINGO"},
            { "Horas", "HORAS"},
            { "Descricao", "DESCRIÇÃO"},
            { "Observacao", "(*) - Batida lançada manualmente   (¨) - Abono Parcial   (^) - Pré-Assinalado"},
            { "Inscricao", "INSCRIÇÃO"},
            { "Titulo", "CARTÃO PONTO"},
            { "LegendasJustificativasTitulo", "LEGENDA DE JUSTIFICATIVAS"},
            { "ContadorPagina", "Página"},
            { "Hora", "HORA"},
            { "Ocorrencia", "OCORRÊNCIA"},
            { "Nome", "NOME"},
            { "EventosTitulo", "EVENTOS"},
            { "Admissao", "ADMISSÃO"},
            { "JustificativasTitulo", "JUSTIFICATIVAS DE ALTERAÇÃO"},
            { "Empresa", "EMPRESA"},
            { "nome_coluna8", "EX100%"},
            { "nome_coluna1", "ENT. 1"},
            { "MensagemAssinaturaDigitalCartaoPonto", ""},
            { "NumeroFolha", "Nº FOLHA"},
            { "CargaDiaria", "CARGA DIÁRIA"},
            { "Ctps", "C.T.P.S."},
        };
    }
}